#!/bin/bash
curl --request POST \
     --header "PRIVATE-TOKEN: $ACCESS_TOKEN" \
     --form "variables[][key]=PORT" --form "variables[][value]=$PORT" \
     --form "variables[][key]=APP_IMAGE" --form "variables[][value]=$APP_IMAGE" \
     --form "variables[][key]=NGINX_IMAGE" --form "variables[][value]=nginx_$CI_COMMIT_BRANCH" \
                                "https://gitlab.com/api/v4/projects/$SLAVE_PROJECT_ID/pipeline?ref=master" | jq .

